import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../widgets/Adaptive_button.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

class NewTransaction extends StatefulWidget {
  final Function addedtx;

  NewTransaction(this.addedtx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  String titleInput = '';
  double amountInput = 0.0;

  DateTime _selectedDate;

  @override
  void initState() {
    print('initstate()');
    super.initState();
  }

  @override
  void didUpdateWidget(NewTransaction oldWidget) {
    print('didupdatewidget()');
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    print('dispose()');
    super.dispose();
  }

  void submitData() async {
    // widget.addedtx(
    //   titleInput,
    //   amountInput,
    //   _selectedDate,
    // );
    var url = 'http://10.0.2.2:3000/user/create-transaction';

    var dt = _selectedDate.toIso8601String();
    var data = {
      "title": titleInput,
      "amount": amountInput,
      "date": dt,
    };

    var response = await http.post(url,
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
        body: json.encode(data));
    //print(response);

    Navigator.of(context).pop();
  }

  void submitCheckData() {
    if (titleInput.isEmpty || amountInput <= 0 || _selectedDate == null) {
      return;
    } else {
      submitData();
    }

    Navigator.of(context).pop();
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime.now(),
    ).then((picked) {
      if (picked == null) {
        return;
      }
      setState(() {
        _selectedDate = picked;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom + 10,
            top: 10,
            right: 10,
            left: 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              CupertinoTextField(),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Title',
                ),
                onChanged: (val) {
                  titleInput = val;
                },
                onSubmitted: (_) => submitData(),
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Amount',
                ),
                keyboardType: TextInputType.number,
                onSubmitted: (_) => submitData(),
                onChanged: (val) => amountInput = double.parse(val),
              ),
              Container(
                height: 70,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        _selectedDate == null
                            ? 'No Date Chosen!'
                            : DateFormat.yMMMd().format(_selectedDate),
                      ),
                    ),
                    AdaptiveButton('Choose Date', _presentDatePicker)
                  ],
                ),
              ),
              RaisedButton(
                onPressed: submitCheckData,
                color: Theme.of(context).primaryColor,
                child: Text(
                  'Add Transaction',
                ),
                textColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
