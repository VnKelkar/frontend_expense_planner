import 'package:flutter/material.dart';
import '../main.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Personel Expense Planner',
      theme: ThemeData(
          primarySwatch: Colors.green,
          accentColor: Colors.red,
          fontFamily: 'Schyler',
          appBarTheme: AppBarTheme(
            textTheme: ThemeData.light().textTheme.copyWith(
                  headline6: TextStyle(
                    fontSize: 30,
                  ),
                ),
          )),
      home: Container(
        child: RaisedButton(
          onPressed: () async{
            var response =await http.get('http://10.0.2.2:3000/user/transactions');
            print(response.body);

            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ExpensePlanner()));
          },
          child: Text('Click here'),
        ),
      ),
    );
  }
}
