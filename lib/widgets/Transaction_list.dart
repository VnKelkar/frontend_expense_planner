import 'package:flutter/material.dart';

import '../models/Transaction.dart';
import '../widgets/Transaction_item.dart';
class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deletedTransaction;

  TransactionList(this.transactions, this.deletedTransaction);

  @override
  Widget build(BuildContext context) {
    return Container(
      // child: ListView(     // listview is combination of column and singlechildscrollview. by default it takes infinite height. here it considers the height of its parent widget
      child: transactions.isEmpty
          ? LayoutBuilder(builder: (ctx, constraints) {
              return Column(
                children: <Widget>[
                  Text(
                    'No transactions added yet',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  const SizedBox(
                    height: 50,
                  ), // sizedbox can be used as separators
                  Container(
                      height: constraints.maxHeight * 0.6,
                      child: Image.asset('assets/images/shopping.png',
                          fit: BoxFit.cover)),
                ],
              );
            })
          : ListView.builder(
              itemBuilder: (ctx, index) {
                return TransactionItem(key: ValueKey(transactions[index].id),transaction: transactions[index], deletedTransaction: deletedTransaction,);
              },
              itemCount: transactions.length,
            ),
    );
  }
}


